
#!/bin/bash

die() {
  echo "$@" 1>&2
  exit 1
}

[ -z $GIT_HOME ] && GIT_HOME=/mnt/c/art-ws.com/git/gitlab.com

APT_UPDATED="NO"
apt_update_once(){
  echo "APT updating..."
  if [ $APT_UPDATED = "NO" ]; then
    sudo apt update
    APT_UPDATED="YES"
  fi
  return 0
}

git_ensure(){
  2>/dev/null 1>&2 git --version || ( apt_update_once ; sudo apt-get install git -y )
  2>/dev/null 1>&2 git --version
}

source_remote(){
  local url="$1"
  shift
  local t=/tmp/tmp_source_remote.sh
  [ -f $t ] && rm $t
  curl -sL "$url" -o $t
  if [ -f $t ]; then
    chmod +x $t
    source $t "$@"
    rm $t
  fi
}

platform_public_install(){
  source_remote "https://gitlab.com/aristsoft/platform-public/-/raw/master/install.sh"
}

artcli_ensure(){
  2>/dev/null 1>&2 art version || platform_public_install
  2>/dev/null 1>&2 art version
}

clone_repos(){
  art git/p/gl-clone platform
  art git/p/gl-clone shagin  
}

setup_home(){
  [ ! -d $GIT_HOME ] && mkdir -p $GIT_HOME
  local repo_path="$GIT_HOME/shagin-public"
  [ ! -d $repo_path ] && git clone git@gitlab.com:aristsoft/shagin-public.git $repo_path 
  local link_path="$HOME/.artrc"
  if [ ! -f $link_path ]; then
    local target_path="$repo_path/artcli/.artrc"
    if [ -f $target_path ]; then
      [ -f $link_path ] && rm $link_path  
      ln -s $target_path $link_path
      echo "Link $link_path created ( $target_path )"
    fi
  fi 
  return 0
}

install(){
  echo "Installing ..."     
  git_ensure && setup_home && artcli_ensure && clone_repos && echo "OK" || echo "Installation failed :("
}

verify(){
  art os/about 
  art git/p/ls
}

install && verify
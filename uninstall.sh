#!/bin/bash

rm_home(){
  [ -f ~/.artrc ] && rm ~/.artrc
}

uninstall(){
  echo "Uninstalling ..."
  local t=~/.tmp.sh
  [ -f $t ] && rm $t
  curl -sL https://raw.githubusercontent.com/art-ws/art-cli/master/purge.sh -o $t
  if [ -f $t ]; then
    chmod +x $t
    source $t
    rm $t
  fi   
  rm_home
  return 0  
}

uninstall
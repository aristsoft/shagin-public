# shagin

Alexander Shagin personal repository

## Setup

### Install

`bash <(curl -sL https://gitlab.com/aristsoft/shagin-public/-/raw/master/install.sh)`

### Uninstall

`bash <(curl -sL https://gitlab.com/aristsoft/shagin-public/-/raw/master/uninstall.sh)`

### Requirements

- [curl](https://www.tutorialspoint.com/unix_commands/curl.htm)
- [Ubuntu](https://ubuntu.com/download)